import React, {Component} from 'react';
import './miniCalculator.less';

class MiniCalculator extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      number: 0
    }
    this.addOne = this.addOne.bind(this);
    this.subOne = this.subOne.bind(this);
    this.multiplyTwo = this.multiplyTwo.bind(this);
    this.divideTwo = this.divideTwo.bind(this);
  }

  render() {
    return (
      <section>
        <div>计算结果为:
          <span className="result"> {this.state.number} </span>
        </div>
        <div className="operations">
          <button onClick={this.addOne}>加1</button>
          <button onClick={this.subOne}>减1</button>
          <button onClick={this.multiplyTwo}>乘以2</button>
          <button onClick={this.divideTwo}>除以2</button>
        </div>
      </section>
    );
  }

  addOne() {
    this.setState({number: this.state.number + 1});
  }

  subOne() {
    this.setState({number: this.state.number - 1});
  }

  multiplyTwo() {
    this.setState({number: this.state.number * 2});
  }

  divideTwo() {
    this.setState({number: this.state.number / 2});
  }


}

export default MiniCalculator;

